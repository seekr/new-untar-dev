@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-body">
                    <div class="col">
                        <a href="{{ route('sale.prediction.step_2') }}" class="btn btn-block btn-primary"> Continue </a>
                    </div>
                </div>
            </div>
            <div class="card">
                <!-- <div class="card-header">{{ __('History Sales Prediction') }} </div> -->

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col"> {{ __('Frekuensi') }} </th>
                                    <th scope="col"> {{ __('Distribusi probabilitas') }} </th>
                                    <th scope="col"> {{ __('Distribusi probabilitas kumulatif') }} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($probabilitas as $prob)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $prob['frekuensi'] }} </td>
                                    <td> {{ $prob['distributed_probabilitas'] }} </td>
                                    <td> {{ $prob['cumulative_probabilitas'] }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
