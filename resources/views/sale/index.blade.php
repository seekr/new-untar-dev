@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @role('sales')
            <div class="card mb-3">
                <div class="card-body">
                    <div class="col">
                        <a href="{{ route('sale.prediction.form') }}" class="btn btn-block btn-primary"> New Sale Prediction </a>

                    </div>
                </div>
            </div>
            @endrole
            <div class="card">
                <div class="card-header">@role(['owner', 'super_admin']) {{ __('Report - ') }} @endrole {{ __('History Sales Prediction') }} </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col"> {{ __('Outlet Name') }} </th>
                                    <th scope="col"> {{ __('Product Name') }} </th>
                                    <th scope="col"> {{ __('Upload Date') }} </th>
                                    <th scope="col"> {{ __('Prediction Date') }} </th>
                                    <th scope="col"> {{ __('Result (%)') }} </th>
                                    @role('sales') <th scope="col">Action</th> @endrole
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($predictions as $predict)
                                    <tr>
                                        <td> {{ $loop->iteration }} </td>
                                        <td> {{ $predict->outlet_name }} </td>
                                        <td> {{ $predict->product_name }} </td>
                                        <td> {{ $predict->created_at }} </td>
                                        <td> {{ $predict->date_prediction }} </td>
                                        <td> {{ $predict->result }} </td>
                                        @role('sales')
                                        <td>
                                            <form method="POST" action="{{ route('sale.prediction.delete', ['id' => $predict->id]) }}">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                            </form>
                                            <!-- <a href="{{ route('sale.prediction.delete', $predict->id) }}" class="btn btn-sm btn-danger"> Delete</a>  -->
                                        </td>
                                        @endrole

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
