@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Sale Prediction Form') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('sale.prediction.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Outlet') }}</label>

                            <div class="col-md-6">
                                <input id="outlet_name" type="text" class="form-control @error('outlet_name') is-invalid @enderror" name="outlet_name" value="{{ old('outlet_name') }}" required autocomplete="outlet_name" autofocus>

                                @error('outlet_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Produk') }}</label>

                            <div class="col-md-6">
                                <input id="product_type" type="text" class="form-control @error('product_type') is-invalid @enderror" name="product_type" value="{{ old('product_type') }}" required autocomplete="product_type" autofocus>

                                @error('product_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Produk') }}</label>

                            <div class="col-md-6">
                                <select name="product_id" id="product_id" class="form-control">
                                    @foreach ($product as $prod)
                                        <option value="{{ $prod->id }}"> {{ $prod->product_name }} - {{ $prod->product_code }} </option>
                                    @endforeach
                                </select>

                                @error('product_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Deret Waktu') }}</label>

                            <div class="col-md-6">
                                <input id="time_range" type="number" class="form-control @error('time_range') is-invalid @enderror" name="time_range" value="{{ old('time_range') }}" required autocomplete="time_range" autofocus>

                                @error('time_range')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('File') }}</label>

                            <div class="col-md-6">
                                <input type="file" name="sales_report" id="sales_report">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection