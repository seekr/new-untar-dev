<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Prediction;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Facades\Excel;

use Maatwebsite\Excel\Concerns\ToArray;

class SaleController extends Controller
{
    public function index()
    {
        $prediction = Prediction::all();
        return view('sale.index')->with('predictions', $prediction);
    }

    public function formPredictionIndex()
    {
        $product = Product::all();
        return view('sale.prediction')->with('product', $product);
    }

    public function formPredictionStore(Request $request)
    {
        $validateData = $request->validate([
            'outlet_name' => 'required',
            'product_type' => 'required',
            'product_id' => 'required',
            'time_range' => 'required',
            'sales_report' => 'required|mimes:xlsx'
        ], [
            'sales_report.required' => 'Please upload an XLSX file.',
            'sales_report.mimes' => 'The uploaded file must be an XLSX file.',
        ]);

        if ($validateData) {
            $fileXlsx = $request->file('sales_report');

            $data = Excel::toArray([], $fileXlsx)[0];
            foreach ($data as &$array) {
                array_shift($array);
            }

            $dataTrain = array_map(function ($array) {
                return $array[1];
            }, array_slice($data, 1));

            $dataTrain = array_slice($dataTrain, 0, ($request->time_range > count($dataTrain) ? count($dataTrain): $request->time_range));

            $distributedProb = $this->countDistributedProbabilitas($dataTrain, 3);

            Cache::forever('predict:distributed-prob', $distributedProb);
            Cache::forever('predict:num-simulation', 1000);
            Cache::forever('predict:store-data', [
                'outlet_name' => $request->outlet_name,
                'product_id' => $request->product_id
            ]);
            Cache::forever('predict:time-range', $request->time_range);

            return redirect()->route('sale.prediction.step_1');
        }
    }

    protected function countDistributedProbabilitas($salesData)
    {
        $sumSalesDaily = array_sum($salesData);

        for ($i = 0; $i < count($salesData); $i++) {
            $dailyReturns[$i]['frekuensi'] = $salesData[$i];
            $dailyReturns[$i]['distributed_probabilitas'] = round($salesData[$i] / $sumSalesDaily, 2);
            // $dailyReturns[] = $salesData[$i] - $salesData[$i - 1];
        }

        $cumulativeProb = 0;
        foreach ($dailyReturns as &$value) {
            $cumulativeProb += $value["distributed_probabilitas"];
            $value["cumulative_probabilitas"] = $cumulativeProb;
        }

        return $dailyReturns;
    }

    protected function generateRandomNumber($min, $max) {
        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }

    protected function anotherMonteCarlo($salesData, $numSimulations)
    {
        // Simulasi Monte Carlo
        $salesPredictions = [];
        $randNumber = [];

        $sumSalesDaily = array_sum($salesData);

        for ($i = 1; $i < count($salesData); $i++) {
            $dailyReturns[$i]['frekuensi'] = $salesData[$i];
            $dailyReturns[$i]['distributed_probabilitas'] = round($salesData[$i] / $sumSalesDaily, 2);
            // $dailyReturns[] = $salesData[$i] - $salesData[$i - 1];
        }

        $cumulativeProb = 0;
        foreach ($dailyReturns as &$value) {
            $cumulativeProb += $value["distributed_probabilitas"];
            $value["cumulative_probabilitas"] = $cumulativeProb;
        }

        // print_r($dailyReturns); die;
        dd($dailyReturns);

        // foreach ($salesData as $lastSale) {
        //     for ($i = 0; $i < $numSimulations; $i++) {
        //         // Menggunakan angka acak berdasarkan persentase perubahan penjualan
        //         $randomPercentage = $this->generateRandomNumber(-0.2, 0.2);
        //         $randNumber[] = $randomPercentage;
        //         $predictedSale = $lastSale * (1 + $randomPercentage);
        //         $salesPredictions[$i][] = $predictedSale;
        //     }
        // }

        // // Statistik hasil prediksi
        // // $averageSale = array_sum($salesPredictions) / count($salesPredictions);
        // // $minSale = min($salesPredictions);
        // // $maxSale = max($salesPredictions);
        // return $salesPredictions;

    }

    protected function monteCarloSalesPrediction($historicalData, $iterations) {
        $totalDays = count($historicalData);
        $dailyReturns = array();

        // Calculate daily returns
        for ($i = 1; $i < $totalDays; $i++) {
            $dailyReturns[] = $historicalData[$i] - $historicalData[$i - 1];
        }

        // Calculate mean and standard deviation of daily returns
        $meanReturn = array_sum($dailyReturns) / $totalDays;
        print_r($dailyReturns);

        $stdDev = sqrt(array_sum(array_map(function ($return) use ($meanReturn) {
            return pow($return - $meanReturn, 2);
        }, $dailyReturns)) / ($totalDays - 1));

        // Generate Monte Carlo simulation
        $simulatedSales = array();
        $lastSale = $historicalData[$totalDays - 1];
        for ($i = 0; $i < $iterations; $i++) {
            $dailyReturnsSimulated = array();
            for ($j = 0; $j < $totalDays - 1; $j++) {
                // Generate a random daily return based on normal distribution
                $randomReturn = $meanReturn + $stdDev * $this->getStandardNormalDistribution();
                $dailyReturnsSimulated[] = $randomReturn;
            }

            // Calculate simulated sales
            $simulatedSale = $lastSale;
            foreach ($dailyReturnsSimulated as $return) {
                $simulatedSale += $return;
            }
            $simulatedSales[] = $simulatedSale;
        }

        return $simulatedSales;
    }

        protected function getStandardNormalDistribution() {
            $u1 = 1.0 - mt_rand() / mt_getrandmax();
            $u2 = 1.0 - mt_rand() / mt_getrandmax();
            return sqrt(-2.0 * log($u1)) * cos(2.0 * M_PI * $u2);
        }
}
