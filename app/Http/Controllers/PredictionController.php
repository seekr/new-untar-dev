<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Prediction;
use App\Models\Product;
use Carbon\Carbon;

class PredictionController extends Controller
{
    public function index()
    {
        return view('sale.index');
    }

    public function stepOnePrediction()
    {
        $distProb = Cache::get('predict:distributed-prob');

        return view('sale.prediction.step_1')->with('probabilitas', $distProb);
    }

    public function stepTwoPrediction()
    {
        // $distProb = Cache::get('predict:distributed-prob');
        // dd(Cache::has('predict:distributed-prob'), Cache::has('predict:num-simulation'), Cache::has('predict:store-data'));
        if (Cache::has('predict:distributed-prob') && Cache::has('predict:num-simulation') && Cache::has('predict:store-data') && Cache::has('predict:time-range')) {
            $distributeProb = Cache::get('predict:distributed-prob');
            $numSimulations = Cache::get('predict:num-simulation');
            $storeData = Cache::get('predict:store-data');

            $salesPredictions = [];
            $randData = [];
            $actualSales = [];
            foreach ($distributeProb as $k => $lastSale) {
                $randomPercentage = $this->getStandardNormalDistribution();
                $randData[] = round($randomPercentage, 2);
                $predictedSale = $lastSale['frekuensi'] * (1 + $randomPercentage);
                $salesPredictions[] = $predictedSale;
                $actualSales[] = $lastSale['frekuensi'];
            }

            $totalPredictions = count($salesPredictions); // Jumlah prediksi penjualan
            $totalAbsolutePercentageError = 0;

            // Menghitung MAPE
            foreach ($salesPredictions as $index => $prediction) {
                $actual = $actualSales[$index];
                $absolutePercentageError = abs(($actual - $prediction) / $actual) * 100;
                $totalAbsolutePercentageError += $absolutePercentageError;
            }

            $mape = round(($totalAbsolutePercentageError / $totalPredictions), 2);
            $result = round(array_sum($salesPredictions) / $totalPredictions, 2);

            try {

                $product = Product::find($storeData['product_id']);

                Prediction::create([
                    'outlet_name' => $storeData['outlet_name'],
                    'product_name' => "{$product->product_name} - ($product->product_code)",
                    'date_prediction' => Carbon::now()->addDays(intval(Cache::get('predict:time-range'))),
                    'result' => "{$result} ({$mape}%)"
                ]);
            } catch (\Exception $e) {
                // abort(500, $e->getMessage());
                dd($e);
            }

            Cache::flush();

            return redirect()->route('sale.index');
        }

        abort(500);
    }

    public function deleteHistory($id)
    {
        // Find the user by ID
        $predict = Prediction::findOrFail($id);

        // Delete the user
        $predict->delete();

        // Optionally, you can redirect or return a response
        return redirect()->route('sale.index')->with('success', 'User deleted successfully');
    }

    protected function generateRandomNumber($min, $max) {
        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }

    protected function getStandardNormalDistribution() {
        $u1 = 1.0 - mt_rand() / mt_getrandmax();
        $u2 = 1.0 - mt_rand() / mt_getrandmax();
        return sqrt(-2.0 * log($u1)) * cos(2.0 * M_PI * $u2);
    }
}
