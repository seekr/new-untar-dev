<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;


class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('role.index')->with('roles', $roles);
    }

    public function formRole()
    {
        return view('role.form');
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'role_name' => ['required', 'string', 'max:255']
        ])->validate();

        try {
            Role::create(['name' => $request->role_name]);
        } catch (\Exception $e) {
            dd($e);
            abort(500, $e->getMessage());
        }

        return redirect()->route('role.index');
    }
}
