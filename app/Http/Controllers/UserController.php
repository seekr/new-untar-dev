<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Spatie\Permission\Models\Role;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('user.index')->with('users', $users);
    }

    public function editForm($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view('user.update')->with('user', $user)->with('roles', $roles);
    }

    public function formUser()
    {
        $roles = Role::all();

        return view('user.create')->with('roles', $roles);
    }

    public function update(Request $req, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $req->name;
        $user->email = $req->email;

        if ($user->roles->first()->id !== $req->role) {
            $user->syncRoles([]);

            $role = Role::findOrFail($req->role);
            $user->assignRole($role->name);
        }

        $user->save();

        return redirect()->back()->with(['message' => 'successfully changed the data']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => ['required']
        ]);

        if (!$validator->validate()) {
            return back()->withInput();
        }

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }

        $roleData = Role::findById($request->role);
        $user->assignRole($roleData->id);

        return redirect()->route('user.index')->withSuccessMessage('Berhasil menambahkan data user');
    }
}
