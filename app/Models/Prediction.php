<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prediction extends Model
{
    use HasFactory;

    protected $fillable = [
        'outlet_name',
        'product_name',
        'date_prediction',
        'result'
    ];

    protected $casts = [
        'date_prediction' => 'datetime'
    ];
}
