<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'users'], function () {
    Route::get('/', [\App\Http\Controllers\UserController::class, 'index'])->name('user.index');
    Route::get('/{id}/edit', [\App\Http\Controllers\UserController::class, 'editForm'])->name('user.form-edit');
    Route::post('/{id}/edit', [\App\Http\Controllers\UserController::class, 'update'])->name('user.update');

    Route::get('/create', [\App\Http\Controllers\UserController::class, 'formUser'])->name('user.form-user');
    Route::post('/store', [\App\Http\Controllers\UserController::class, 'store'])->name('user.store');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', [\App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
    Route::get('/create', [\App\Http\Controllers\RoleController::class, 'formRole'])->name('role.form-role');
    Route::post('/store', [\App\Http\Controllers\RoleController::class, 'store'])->name('role.store');
});

Route::get('/report', [\App\Http\Controllers\SaleController::class, 'index'])->name('report.index');

Route::group(['prefix' => 'sales'], function () {
    Route::get('/', [\App\Http\Controllers\SaleController::class, 'index'])->name('sale.index');
    Route::group(['prefix' => 'prediction'], function () {
        Route::get('/', [\App\Http\Controllers\SaleController::class, 'predictionIndex'])->name('sale.prediction.index');
        Route::post('/', [\App\Http\Controllers\SaleController::class, "formPredictionStore"])->name('sale.prediction.store');
        Route::get('/step_1', [\App\Http\Controllers\PredictionController::class, 'stepOnePrediction'])->name('sale.prediction.step_1');
        Route::get('/step_2', [\App\Http\Controllers\PredictionController::class, 'stepTwoPrediction'])->name('sale.prediction.step_2');
        Route::get('/form', [\App\Http\Controllers\SaleController::class, "formPredictionIndex"])->name('sale.prediction.form');
        Route::delete('/{id}', [\App\Http\Controllers\PredictionController::class, 'deleteHistory'])->name('sale.prediction.delete');
    });
});
