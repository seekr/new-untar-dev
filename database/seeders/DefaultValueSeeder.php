<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\Product;
use App\Models\User;
use Spatie\Permission\Models\Role;

class DefaultValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'product_name' => "Extra Joss",
            'product_code' => "2A0U",
            'product_price' => 6000
        ]);

        Role::create(['name' => 'super_admin']);
        Role::create(['name' => 'sales']);
        Role::create(['name' => 'owner']);

        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'super_admin@gmail.com',
            'password' => Hash::make('admin123'),
        ]);

        $user->assignRole('super_admin');
    }
}
